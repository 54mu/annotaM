configfile: "config.yaml"


transcriptome = config["transcriptome"]
basename = config["basename"]
cores = config["cores"]
gencode = config["gencode"]
uniprot_database = config["uniprot_sprot"]
orthodb = config["ortho_db"]
pfam_db = config["pfam_db"]
signalp = config["signalp"]

gencode_dictionary = {
    1 : "Universal", 
    10 : "Euplotid"
    }

gencode_string = gencode_dictionary[gencode]

rule all:
    input:
        expand("{basename}.transcript_annotation.tsv", basename = basename),
        expand("{basename}.protein_annotation.tsv", basename = basename)

rule download_uniprot:
    output:
        "downloads/uniprot_sprot.fasta"
    shell:
        "curl https://ftp.uniprot.org/pub/databases/uniprot/knowledgebase/complete/uniprot_sprot.fasta.gz | gunzip > {output}"

rule download_pfam:
    output:
    	"downloads/Pfam-A.hmm"
    shell:
    	"curl https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz | gunzip > {output}"

rule index_uniprot:
    input:
        rules.download_uniprot.output
    output:
        "local_dbs/uniprot_sprot.dmnd"
    shell:
        "diamond makedb -d {output} --in {input}"

rule annothate:
    input:
        expand("{basename}.transcript_annotation.tsv", basename = basename),
        expand("{basename}.protein_annotation.tsv", basename = basename)

rule blastx_uniprot:
    input: 
        query = expand("{transcriptome}", transcriptome = transcriptome),
        db = rules.index_uniprot.output
    output: expand("{basename}.blastx.uniprot.tbl", basename = basename)
    shell:
        "diamond blastx -d {input.db} -p {cores} -f 6 qseqid sseqid qstart qend -e 1e-3 -k 1 --query-gencode {gencode} -q {input.query} -o {output} --more-sensitive"
    
rule blastx_orthodb:
    input: expand("{transcriptome}", transcriptome = transcriptome)
    output: expand("{basename}.blastx.orthodb.tbl", basename = basename)
    shell:
        "diamond blastx -d {orthodb} -p {cores} -f 6 qseqid sseqid qstart qend -e 1e-3 -k 1 --query-gencode {gencode} -q {input} -o {output} --more-sensitive"

rule transdecoder:
    input: expand("{transcriptome}", transcriptome = transcriptome)
    output: expand("{basename}_transdecoder/longest_orfs.pep", basename = basename)
    shell:
        "TransDecoder.LongOrfs -t {input} -O {basename}_transdecoder -G {gencode_string}"
        
rule blastp_uniprot:
    input: 
        query = expand("{basename}_transdecoder/longest_orfs.pep", basename = basename),
        db = rules.index_uniprot.output
    output: expand("{basename}.blastp.uniprot.tbl", basename = basename)
    shell:
        "diamond blastp -d {input.db} -p {cores} -f 6 qseqid sseqid -e 1e-3 -k 1 --query-gencode {gencode} -q {input.query} -o {output} --more-sensitive"
        
rule blastp_orthodb:
    input: expand("{basename}_transdecoder/longest_orfs.pep", basename = basename)
    output: expand("{basename}.blastp.orthodb.tbl", basename = basename)
    shell:
        "diamond blastp -d {orthodb} -p {cores} -f 6 qseqid sseqid -e 1e-3 -k 1 --query-gencode {gencode} -q {input} -o {output} --more-sensitive"

rule hmmer:
    input: 
    	seqs = expand("{basename}_transdecoder/longest_orfs.pep", basename = basename),
	db = rules.download_pfam.output
    output: expand("{basename}.pfam.tbl", basename = basename)
    shell:
        "hmmsearch --cpu {cores} --domtblout {basename}.pfam.temp.tsv {input.db} {input.seqs} && "
        "awk '!/#/' {basename}.pfam.temp.tsv| sed 's/\s\+/\t/g' | cut -f1,4,5 > {output}"
        " && rm {basename}.pfam.temp.tsv"

rule signalp5:
    input: expand("{basename}_transdecoder/longest_orfs.pep", basename = basename)
    output: expand("{basename}.signalp5.tbl", basename = basename)
    shell:
        "{signalp} -fasta {input} && grep 'SP(Sec/SPI)' longest_orfs_summary.signalp5 | cut -f1,3,5 > {output} && rm -f longest_orfs_summary.signalp5"

rule get_blastp_GO:
    input: expand("{basename}.blastp.uniprot.tbl", basename = basename)
    output: expand("{basename}.blastp.uniprot.GO.tbl", basename = basename)
    params:
        core_script=workflow.source_path("./core/2GOp.py")
    shell:
        "python3 {params.core_script} {basename} {cores}"

rule get_blastx_GO:
    input: expand("{basename}.blastx.uniprot.tbl", basename = basename)
    output: expand("{basename}.blastx.uniprot.GO.tbl", basename = basename)
    params:
        core_script=workflow.source_path("./core/2GOx.py")
    shell:
        "python3 {params.core_script} {basename} {cores}"

rule ortho_transX:
    input: 
        expand("{basename}.blastx.orthodb.tbl", basename = basename)
    output:
        expand("{basename}.blastx.orthodb.translated.tbl", basename = basename)
    params:
        core_script=workflow.source_path("./core/translate_orthoX.py")
    shell:
        "python3 {params.core_script} {basename} {cores}"
        
rule ortho_transP:
    input: 
        expand("{basename}.blastp.orthodb.tbl", basename = basename)
    output:
        expand("{basename}.blastp.orthodb.translated.tbl", basename = basename)
    params:
        core_script=workflow.source_path("./core/translate_orthoP.py")
    shell:
        "python3 {params.core_script} {basename} {cores}"
        
rule ortho_trans:
    input:
        expand("{basename}.blastp.orthodb.translated.tbl", basename = basename),
        expand("{basename}.blastx.orthodb.translated.tbl", basename = basename)

rule merge:
    input:
        expand("{basename}.blastx.uniprot.GO.tbl", basename = basename),
        expand("{basename}.blastp.uniprot.GO.tbl", basename = basename),
        expand("{basename}.signalp5.tbl", basename = basename),
        expand("{basename}.pfam.tbl", basename = basename),
        expand("{basename}.blastp.orthodb.translated.tbl", basename = basename),
        expand("{basename}.blastx.orthodb.translated.tbl", basename = basename)
    output:
        expand("{basename}.transcript_annotation.tsv", basename = basename),
        expand("{basename}.protein_annotation.tsv", basename = basename)
    params:
        core_script=workflow.source_path("./core/annotaM.py")
    shell:
        "python3 {params.core_script} {basename} {cores} {uniprot_database}"
        
#########
# TO DO #
#########

rule rnammer:
    input:
        expand("{transcriptome}", transcriptome = transcriptome)
    output:
        expand("{transcriptome}.rnammer.gff", transcriptome = transcriptome)
    shell:
        "/mnt/DATA/Software/rnammer_support/RnammerTranscriptome.pl --transcriptome {transcriptome} --path_to_rnammer ~/Software/RNAmmer/rnammer --org_type euk"
        
