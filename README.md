# annot.aM

## table of contents

[ TOC ]

## introduction

This is a fast and extendible transcriptome **annotation** pipeline. It searches each transcript in multiple databases with multiple software. The final output consists in two tables, one for transcripts (the main one) and one for the translated transcripts. It is based on Snakemake, which grants a high degree of parallelism. It also makes a large use of web APIs.

## How annotaM works

annotaM is like [King Crimson] it just works.  
The input transcriptome is searched in a blastx searche against UniprotSprot, allowing the assignation of uniprot ids to the transcripts via the best hit. Such uniprot ids are then used to query the Gene Ontology in order to obtain GO terms and are used to retrieve relevant pathways from reactome database.  
The input transcriptome is also translated with transdecoder and the resulting sequences are used for a HMMER search against the PFAM database in order to retrieve functional domains.  
Signalp5 (optional) is used to identify signal peptides in order to identify eventual secreted peptides.  

All intermediate and final output files are tsv formatted, allowing easy analisys exetensibility and portability of data. The snakemake backbone allows customization, implementation and depletion of steps of the wole pipeline. 

## dependendecies

### Software

* Python3
* [Snakemake](https://snakemake.readthedocs.io/en/stable/)
* [Pandas](https://pandas.pydata.org/)
* [diamond](http://www.diamondsearch.org/index.php)
* [HMMER](http://hmmer.org/)
* [signalp](http://www.cbs.dtu.dk/services/SignalP/)

### Databases

* [Uniprot-Swissprot](https://www.uniprot.org/)
* [PFAM](http://pfam.xfam.org/)
* [orthoDBv9](https://www.orthodb.org/) (from the desired taxa)

Databases in fasta format need to be first indexed with diamond!

## installation 

1. Clone this repository
2. Install the dependencies
3. configure paths in config.yaml

## how to use annot.aM

We use snakemake to manage all the scripts and software in the pipeline, in order to properly configure each run it is required to edit and save a config.yml. A sample config.yml is provided in the repository. 

annotaM makes use of webAPIs in order to provide the most recent annotations while trying to not take up too much disk space with database that get updated daily. Make sure to use it with an internet connection. Databases are not all in-the-cloud yet so you may need to update the static databases every now and then.

1. copy the config.yaml file to the working directory
2. edit the config file accordingly
3. run
```bash
snakemake -s /path/to/annotaM_folder/Snakefile --config 
```
4. ????
5. profit

You can use the ```-j n``` option to specify the number of jobs that you would like to run in parallel. Note that this is not related to the number of threads specified in the config file, but tells snakemake to run *n* "runnable" jobs in parallel if all the input files are available.   

The output files will be available as {basename}.transcript_annotation.tsv and {basename}.protein_annotation.tsv. 

## to be implemented

- [ ] automatic updater of databases
- [ ] automatic diamond database generation
- [ ] orthoDB v10
- [ ] installer of dependencies
- [ ] automatic generation of the config file.
- [ ] automatic feature enrichemnt test based on a subset
- [ ] fully-functional conda environment
- [ ] PFAM clans

## known issues

> using too many threads in the config files crashes the webAPI 

solution:

decrease the number of cores in the config file at leas for this step

## how to cite annotaM

If you use this pipeline for you research papers please cite us as follows:

the publication is work-in-progress, for now just cite this repository!
