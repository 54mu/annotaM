#!/usr/bin/env python
import pandas as pd
import requests, json
import sys, time
import numpy as np
from multiprocessing import cpu_count, Pool

basename = sys.argv[1]
cores = int(sys.argv[2])

partitions = cores

def parallelize(data, func):
    data_split = np.array_split(data, partitions)
    pool = Pool(cores)
    data = pd.concat(pool.map(func, data_split))
    pool.close()
    pool.join()
    return data

def get_ortho_name(s):
    s = str(s)
    requestURL = "https://www.orthodb.org/ogdetails?id={}".format(s)
    print ("searching for {}".format(s), end = "\r")
    r = requests.get(requestURL, headers={"Accept":"application/json"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    response = json.loads(r.text)
    data = response["data"]
    try:
        source = list(data.keys())[0]
        desc = data[source][0]["description"]
        name = data[source][0]["id"]
    except:
        desc = name = source = ""
    return desc, name, source

def parallel_get_ortho_names(df):
    df["ortho_description"], df["ortho_name"] , df["ortho_source"]= zip(*df["ortho_unique_ID"].apply(lambda x: get_ortho_name(x)))
    return df

tic = time.time()


# silencing this code block, testing for downladless annotation

"""print("building OGmap...")
#build OG map
OGmap = {}
with open("/mnt/DATA/Shared/Databases/orthodb/odb10v0_OGs.tab") as f1:
    for line in f1:
        ln = line[:-1].split("\t")
        print ("mapping {}".format(ln[0]), end = "\r")
        try:
            OGmap[ln[0]] += "; {}".format(ln[2])
        except:
            OGmap[ln[0]] = ln[2]
"""
print("reading database...")




ortho_x = pd.read_csv("{}.blastx.orthodb.tbl".format(basename), sep = "\t", names = ["transcript_id", "ortho_unique_ID", "blastx_start", "blastx_end"])
ortho_x = ortho_x[["transcript_id", "ortho_unique_ID"]]
print("building base map")
#build base map
basemap_list = []


# silencing this code block, testing for downladless annotation

"""
with open("/mnt/DATA/Shared/ramdisk/odb10v0_OG2genes.tab") as base:
    searchset = set(ortho_x.ortho_unique_ID)
    for line in base:
        ln = line[:-1].split("\t")
        print ("searching {}".format(ln[1]), end = "\r")
        rowd = {}
        if len(searchset) == 0:
            break
        if ln[1] in searchset:
            print ("\nfound {} {} more to go\n".format(ln[1], len(searchset)))
            rowd["ortho_unique_ID"] = ln[1]
            rowd["ortho_gene_name"] = ln[0]
            basemap_list.append(rowd)
            searchset.remove(ln[1])
        else:
            pass

map_df = pd.DataFrame(basemap_list)
try:
    print(len(map_df.ortho_gene_name))
except:
    map_df["ortho_unique_ID"] = ""
    map_df["ortho_gene_name"] = ""
    map_df["ortho_gene_description"] = ""
map_df["ortho_gene_description"] = map_df.ortho_gene_name.apply(lambda x: OGmap[x])        
"""

print("========orthoX=========\n")

#ortho_x=ortho_x.merge(map_df, on = "ortho_unique_ID", how = "left")
ortho_x = parallelize(ortho_x, parallel_get_ortho_names)



toc=time.time()
print("\ntook {} hours, {} minutes, {} seconds\n".format((toc-tic)/3600, ((toc-tic)%3600)/60, (toc-tic)%60))

ortho_x.to_csv("{}.blastx.orthodb.translated.tbl".format(basename), sep = "\t", index = False)
