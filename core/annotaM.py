#!/usr/bin/env python
# coding: utf-8

import time
import pandas as pd
import requests, sys, json
import numpy as np
from multiprocessing import cpu_count, Pool
basename = sys.argv[1]
cores = int(sys.argv[2])
uniprot_fasta_file = sys.argv[3].replace(".dmnd", ".fasta")

partitions = cores #Define as many partitions as you want

def parallelize(data, func):
    data_split = np.array_split(data, partitions)
    pool = Pool(cores)
    data = pd.concat(pool.map(func, data_split))
    pool.close()
    pool.join()
    return data


pfam = pd.read_csv("{}.pfam.tbl".format(basename), sep = "\t", names = ["prot_id", "desc", "pfam_id"])

print("loaded pfam")

pfam_memoy = pfam

pfam["pfam"] = pfam[["prot_id", "pfam_id"]].groupby("prot_id").transform(lambda x: '; '.join(set(x)))

pfam = pfam[["prot_id", "pfam"]].drop_duplicates()

print("fixed pfam")
print(pfam.head())

blastp = pd.read_csv("{}.blastp.uniprot.GO.tbl".format(basename), sep = "\t")

print("loaded blastp")
print(blastp.head())

blastx = pd.read_csv("{}.blastx.uniprot.GO.tbl".format(basename), sep = "\t")

print("loaded blastx")
print(blastx.head())


sigP = pd.read_csv("{}.signalp5.tbl".format(basename), sep = "\t")

sigP.columns = ["prot_id", "signalp_score", "CS_Position"]


print("loaded sigP")
print(sigP.head())



#final_table_prot = blastp.merge(pfam, on = "prot_id", how = "outer").merge(sigP, on = "prot_id", how = "outer")
ortho_p = pd.read_csv("{}.blastp.orthodb.translated.tbl".format(basename), sep = "\t")
ortho_x = pd.read_csv("{}.blastx.orthodb.translated.tbl".format(basename), sep = "\t")

print("loaded ortho")
print(ortho_p.head())



uniprot_names = {}
with open(uniprot_fasta_file, "r") as uniprot_fasta:
    for line in uniprot_fasta:
        if line[0] == ">":
            data = line[1:-1].split(" ")
            uniprot_names[data[0].split("|")[1]] = " ".join(data[1:])
        else:
            pass

def get_up_names(s):
    try:
        #print(uniprot_names[s], end = "\r")
        return uniprot_names[s]
    except:
        return "unknown"


print("setting uniprot names")
blastp["UniProt_name"] = blastp.uniprot_id.apply(get_up_names)
blastx["UniProt_name"] = blastx.uniprot_id.apply(get_up_names)

print(blastp.head())
print(blastx.head())

print("merging")

reactome_1 = pd.read_csv("https://reactome.org/download/current/UniProt2Reactome.txt", sep = "\t", names = ["uniprot_id", "reactome_pathway_id", "URL", "pathway_name", "evidence_code", "species"])

#pfam["pfam"] = pfam[["prot_id", "pfam_id"]].groupby("prot_id").transform(lambda x: '; '.join(x))
reactome_1["reactome_pathway_name"] = reactome_1.groupby("uniprot_id")["pathway_name"].transform(lambda x: '; '.join(x))
reactome_1["reactome_pathway_id"] = reactome_1.groupby("uniprot_id")["reactome_pathway_id"].transform(lambda x: '; '.join(x))
reactome = reactome_1[["uniprot_id", "reactome_pathway_id", "reactome_pathway_name"]].sort_values("uniprot_id").drop_duplicates()

print("processed reactome table")
print("recovering pfam ids for transcripts")

part_prot_table = pfam_memoy
part_prot_table["transcript_id"]= part_prot_table.prot_id.apply(lambda x: ".".join(x.split(".")[:-1]))
#part_prot_table = part_prot_table[["transcript_id", "pfam_id"]]
part_prot_table = part_prot_table.sort_values(by = ["transcript_id"])
part_prot_table["pfam"] = part_prot_table[["prot_id", "pfam_id"]].groupby("prot_id").transform(lambda x: '; '.join(set(x)))
part_prot_table = part_prot_table[["transcript_id", "pfam"]]
part_prot_table = part_prot_table.sort_values(by = ["transcript_id", "pfam"])
part_prot_table = part_prot_table.drop_duplicates()

print("converting to string")
blastx.uniprot_id = blastx.uniprot_id.astype(str)
print("creating results")



final_table_prot = blastp.merge(pfam, on = "prot_id", how = "outer").merge(sigP, on = "prot_id", how = "outer").merge(ortho_p, on = "prot_id", how = "outer").merge(reactome, on = "uniprot_id", how = "left")
final_table_trans = blastx.drop(["blastx_start", "blastx_end"], axis = 1).merge(part_prot_table, on = "transcript_id", how = "outer").merge(ortho_x, on = "transcript_id", how = "outer").merge(reactome, on = "uniprot_id", how = "left")



final_table_prot.drop_duplicates().to_csv("{}.protein_annotation.tsv".format(basename), sep = "\t", index = False)
final_table_trans.drop_duplicates().to_csv("{}.transcript_annotation.tsv".format(basename), sep = "\t", index = False)
