 #!/usr/bin/env python
import pandas as pd
import requests, json
import sys, time
import numpy as np
from multiprocessing import cpu_count, Pool

basename = sys.argv[1]
cores = int(sys.argv[2])

partitions = cores

def parallelize(data, func):
    data_split = np.array_split(data, partitions)
    pool = Pool(cores)
    data = pd.concat(pool.map(func, data_split))
    pool.close()
    pool.join()
    return data

def get_ortho_name(s):
    s = str(s)
    requestURL = "https://www.orthodb.org/ogdetails?id={}".format(s)
    print ("searching for {}".format(s), end = "\r")
    r = requests.get(requestURL, headers={"Accept":"application/json"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    response = json.loads(r.text)
    data = response["data"]
    try:
        source = list(data.keys())[0]
        desc = data[source][0]["description"]
        name = data[source][0]["id"]
    except:
        desc = name = source = ""
    return desc, name, source

def parallel_get_ortho_names(df):
    df["ortho_description"], df["ortho_name"] , df["ortho_source"]= zip(*df["ortho_unique_ID"].apply(lambda x: get_ortho_name(x)))
    return df

ortho_p = pd.read_csv("{}.blastp.orthodb.tbl".format(basename), sep = "\t", names = ["prot_id", "ortho_unique_ID"])
ortho_x = pd.read_csv("{}.blastx.orthodb.tbl".format(basename), sep = "\t", names = ["transcript_id", "ortho_unique_ID"])

print("========orthoP=========\n")

tic = time.time()
ortho_p = parallelize(ortho_p, parallel_get_ortho_names)
toc= time.time()
print("\ntook {} hours, {} minutes, {} seconds\n".format((toc-tic)/3600, ((toc-tic)%3600)/60, (toc-tic)%60))

print("========orthoX=========\n")

tic = time.time()
ortho_x = parallelize(ortho_x, parallel_get_ortho_names)
toc= time.time()
print("\ntook {} hours, {} minutes, {} seconds\n".format((toc-tic)/3600, ((toc-tic)%3600)/60, (toc-tic)%60))

ortho_p.to_csv("{}.blastp.orthodb.translated.tbl".format(basename), sep = "\t", index = False)
ortho_x.to_csv("{}.blastx.orthodb.translated.tbl".format(basename), sep = "\t", index = False)
