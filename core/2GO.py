#!/usr/bin/env python
import pandas as pd
import requests, json
import sys, time
import numpy as np
from multiprocessing import cpu_count, Pool

basename = sys.argv[1]
cores = int(sys.argv[2])

partitions = cores

def parallelize(data, func):
    data_split = np.array_split(data, partitions)
    pool = Pool(cores)
    data = pd.concat(pool.map(func, data_split))
    pool.close()
    pool.join()
    return data

def smart_getGO(s):
    goset = set()
    #golist = []
    requestURL = "https://www.ebi.ac.uk/QuickGO/services/annotation/search?geneProductId={}".format(s)
    print("searching for {}".format(s), end = "\r")
    r = requests.get(requestURL, headers={ "Accept" : "application/json"})
    if not r.ok:
      r.raise_for_status()
      sys.exit()
    response = json.loads(r.text)
    for i in response["results"]:
        goset.add(i["goId"])
    return "; ".join(goset)

def get_GO(df):
    df["GO"] = df["uniprot_id"].apply(smart_getGO)
    return df

def get_id(s):
    try:
        return s.split("|")[1]
    except:
        return ""

blastp = pd.read_csv("{}.blastp.uniprot.tbl".format(basename), sep = "\t", names = ["prot_id", "uniprot_hit"])
blastp = blastp.drop_duplicates()

print("\n=========blastP==========\n")

tic = time.time()

blastp["uniprot_id"] = blastp.uniprot_hit.apply(get_id)
blastp2 = parallelize(blastp, get_GO)
blastp = blastp2

toc= time.time()
print("\ntook {} hours, {} minutes, {} seconds\n".format((toc-tic)/3600, ((toc-tic)%3600)/60, (toc-tic)%60))

print("\n=========blastX==========\n")

tic = time.time()
blastx = pd.read_csv("{}.blastx.uniprot.tbl".format(basename), sep = "\t", names = ["transcript_id", "uniprot_hit"])
blastx = blastx.drop_duplicates()
blastx["uniprot_id"] = blastx.uniprot_hit.apply(get_id)
blastx2 = parallelize(blastx, get_GO)
blastx = blastx2



toc= time.time()
print("\ntook {} hours, {} minutes, {} seconds\n".format((toc-tic)/3600, ((toc-tic)%3600)/60, (toc-tic)%60))


print("Saving tables...")
blastp.to_csv("{}.blastp.uniprot.GO.tbl".format(basename), sep = "\t", index = False)
blastx.to_csv("{}.blastx.uniprot.GO.tbl".format(basename), sep = "\t", index = False)




